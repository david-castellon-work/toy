"use strict";

var _express = _interopRequireDefault(require("express"));

var _app = _interopRequireDefault(require("./app.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var server = (0, _express["default"])();
var port = process.env.PORT || '3000';
server.use('/toy', _app["default"]);
server.listen(port);

function test_redirect(req, res) {
  res.redirect('/toy');
}