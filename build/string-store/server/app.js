"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _path = _interopRequireDefault(require("path"));

var _express = _interopRequireDefault(require("express"));

var _word = _interopRequireDefault(require("./word"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

// import mongoose		from 'mongoose' ;
var app = (0, _express["default"])(); //	mongoDB	= 'mongodb+srv://dcastellon:Awesome0001@cluster0-0qm90.azure.mongodb.net/Toys?retryWrites=true' ;
// mongoose . connect ( mongoDB , { useNewUrlParser : true } ) ;

app.use(_express["default"].urlencoded({
  extended: false
}));
app.use(_express["default"].json());
app.get('/find/:word', function (req, res, next) {
  var search = new RegExp(req.params.word);

  _word["default"].find({
    _id: search
  }).exec(function (err, text_list) {
    if (err) {
      return err;
    }

    var matches = [];
    text_list.forEach(function (value, index, array) {
      matches.push(value._id);
    });
    res.send(matches);
  });
});
app.post('/create', function (req, res, next) {
  var word = req.body.word;
  console.log('word: ' + word);

  _word["default"].create({
    _id: word
  }, function (err, something) {
    if (err) {
      if (err.code == 11000) res.send(word + ' is already in database');
      return err;
    }

    res.send(word + ' saved in database!');
  });
});
var _default = app;
exports["default"] = _default;