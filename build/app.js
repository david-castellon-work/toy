"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _path = _interopRequireDefault(require("path"));

var _express = _interopRequireDefault(require("express"));

var _mongoose = _interopRequireDefault(require("mongoose"));

var _morgan = _interopRequireDefault(require("morgan"));

var _app = _interopRequireDefault(require("./string-store/server/app"));

var _app2 = _interopRequireDefault(require("./login/server/app"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var app = (0, _express["default"])();
var mongoDB = 'mongodb+srv://dcastellon:Awesome0001@cluster0-0qm90.azure.mongodb.net/Toys?retryWrites=true'; // const mongoDB = 'mongodb://159.65.191.124:27017/Toys?retryWrites=true' ;

_mongoose["default"].connect(mongoDB, {
  useNewUrlParser: true
});

app.set('view engine', 'pug');
app.set('views', _path["default"].join(__dirname, 'views'));
app.use('/public', _express["default"]["static"](__dirname));
app.use(_express["default"].urlencoded({
  extended: false
}));
app.use(_express["default"].json());
app.use((0, _morgan["default"])('dev'));
app.get('/documentation', function (req, res) {
  res.render('toy');
}); // app . use ( '/' , ( ) => console . log ( 'inside /:' ) ) ;

app.use('/word-store', _app["default"]);
app.use('/users', _app2["default"]);
var _default = app;
exports["default"] = _default;

function test_redirect(req, res) {
  res.redirect('/toy/documentation');
}