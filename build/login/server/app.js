"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _path = _interopRequireDefault(require("path"));

var _express = _interopRequireDefault(require("express"));

var _user = _interopRequireDefault(require("./user"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var app = (0, _express["default"])();
app.use(_express["default"].urlencoded({
  extended: false
}));
app.use(_express["default"].json()); // Handles User Login
// I need to add the password part, but I'm just checking at the moment.

app.post('/find', function (req, res, next) {
  var username = new RegExp(req.body.username);

  _user["default"].find({
    _id: search
  }).exec(function (err, matches) {
    if (err) return err;

    if (matches[0] == search) {
      console.log('User in database!');
      res.send('User in database!');
    } else {
      console.log('User not in database!');
      res.send('User not in database!');
    }
  });
}); // Handle User Creation
// I need to be able to create the user.

app.post('/create', function (req, res, next) {
  console.log('in /users/create');
  var username = req.body.username;
  var password = req.body.password;
  console.log('username: ' + username);
  console.log('password: ' + password);

  _user["default"].create({
    _id: username,
    password: password
  }, function (err, something) {
    if (err) {
      console.log('something: ' + something);
      if (err.code == 11000) res.send(username + ' is already in database');
      return err;
    }

    res.send(username + ' saved in database!');
  });
});
var _default = app;
exports["default"] = _default;