# Toy

I am going to implement the forms using react, then I will transition to formik
because it is the tool for the job.

These are just some simple apps that I built while learning
new technologies.

## Components

### Timer

A clock that is updated every second.
I'm gonna change this to a timer.

### Tic-Tac-Toe

An implementation of tic-tac-toe.

### String Store

A search bar that records new entries in a database on form submission
and also fetches words that closely match on button releases.

### Calculator

Calculator implementation.

### Login

A form that allows users to store information in a database.

## NPM Commands:
	// This command builds the app.
	npm run build
	// This command builds and starts the apps in watch mode.
	npm run test
