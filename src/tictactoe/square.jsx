import React from 'react' ;
export default props => (
	<div
		className = 'tictactoe-square'
		onClick = { props . onClick }
	>
		{ props . value }
	</div>
) ;
