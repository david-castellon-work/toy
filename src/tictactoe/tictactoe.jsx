import calculateWinner from './calculate_winner.jsx' ;
import React from 'react' ;
import Board from './board.jsx' ;

export default class Game extends React . Component {
	constructor ( props ) {
		super ( props ) ;
		this . state = {
			history : [ { squares : Array ( 9 ) . fill ( null ) } ] ,
			stepNumber : 0 ,
			xIsNext : true ,
		} ;
	}
	jumpTo ( step ) {
		this . setState ( {
			stepNumber : step ,
			xIsNext : ( step % 2 ) === 0 ,
		} ) ;
	}
	handleClick ( i ) {
		const history = this . state . history . slice ( 0 , this . state . stepNumber + 1 ) ;
		const current = history [ history . length - 1 ] ;
		const squares = current . squares . slice ( ) ;

		if ( calculateWinner ( squares ) || squares [ i ] ) {
			return ;
		}

		squares [ i ] = this . state . xIsNext ? 'X' : 'O' ;

		this . setState ( {
			history : history . concat ( [ {
				squares : squares ,
			} ] ) ,
			stepNumber : history . length ,
			xIsNext : ! this . state . xIsNext ,
		} ) ;
	}
	render ( ) {
		const history = this . state . history ;
		const current = history [ this . state . stepNumber ] ;
		const winner = calculateWinner ( current . squares ) ;

		const moves = history . map ( ( step , move ) => {
			const desc = move ?
				'Go to move #' + move :
				'Go to game start' ;
			return (
				<li
					className = 'tictactoe-history'
					key={move}
				>
					<button
						className = 'tictactoe-history'
						onClick={()=>this.jumpTo(move)}
					>{desc}</button>
				</li>
			) ;
		} ) ;

		let status ;
		if ( winner ) {
			status = 'Winner: ' + winner ;
		} else {
			status = 'Next player: ' + ( this . state . xIsNext ? 'X' : 'O' ) ;
		}
		// <li key={user.id}>{user.name}: {user.taskCount} tasks left</li>
		return (
			<div className='tictactoe'>
				<div className='tictactoe-board'>
					<div className='tictactoe-turn'>{status}</div>
					<Board
						squares = { current . squares }
						onClick = { ( i ) => this . handleClick ( i ) }
					/>
				</div>
				<ol className='tictactoe-moves'>{moves}</ol>
			</div>
		) ;
	}
}
