import TicTacToe from './tictactoe.jsx' ;
import React from 'react' ; // This is impored for its side affects.
import ReactDOM from 'react-dom' ;

ReactDOM . render (
	<TicTacToe /> ,
	document . getElementById ( 'tictactoe' )
) ;
