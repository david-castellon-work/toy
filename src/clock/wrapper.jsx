import React from 'react' ;
import ReactDOM from 'react-dom' ;
import Clock from './clock.jsx' ;

ReactDOM . render (
	<Clock /> ,
	document . getElementById ( 'clock' )
) ;
