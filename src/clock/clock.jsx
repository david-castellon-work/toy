import React from 'react' ;
import ReactDOM from 'react-dom' ;

// React sets up props and state, but you can add additional fields.
export default class Clock extends React . Component {
	// The constructor just initializes.
	constructor ( props ) {
		// class components should always call super with props.
		super ( props ) ;
		this . state = {
			start : new Date ( ) ,
			current : new Date ( ) ,
		} ;
	}
	restart_initial ( ) {
		this . setState ( ( state , props ) => ( {
			start : new Date ( )
		} ) ) ;
	}
	tick ( ) {
		this . setState ( {
			current : new Date ( )
		} ) ;
	}
	passed ( ) {
		let time = this . state . current - this . state . start ;
		time = time / 100 ;
		time = Math . trunc ( time ) ;
		time = time / 10 ;
		return time . toFixed ( 1 ) ;
	}
	componentDidMount ( ) {
		this . timerID = setInterval (
			( ) => ( this . tick ( ) ) ,
			100
		) ;
	}
	componentWillUnmount ( ) {
		clearInterval ( this . timerID ) ;
	}
	render ( ) {
		return (
			<h2>{ this . passed ( ) }</h2>
		) ;
	}
}
