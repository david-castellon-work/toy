import React from 'react' ;
import ResultComponent from './ResultComponent.jsx' ;
import KeyPadComponent from './KeyPadComponent.jsx' ;

export default class App extends React . Component {
	constructor ( ) {
		super ( ) ;
		this . state = {
			result : ''
		}
	}
	onClick = button => {
		if ( button === '=' ) {
			this . calculate ( ) ;
		} else if ( button === 'C' ) {
			this . reset ( ) ;
		} else if ( button === 'CE' ) {
			this . backspace ( ) ;
		} else {
			// this arrow notation returns the obj
			// with param result.
			this . setState ( ( state , props ) => ( {
				result : this . state . result + button
			} ) ) ;
		}
	} ;
	calculate = ( ) => {
		try {
			this . setState ( ( state , props ) => ( {
				result : ( eval ( this . state . result ) || '' ) + ''
			} ) ) ;
		} catch ( e ) {
			this . setState ( {
				result : 'error'
			} ) ;
		}
	} ;
	reset = ( ) => {
		this . setState ( {
			result : ''
		} )
	} ;
	backspace = ( ) => {
		this . setState ( ( state , props ) => ( {
			result : this . state . result . slice ( 0 , -1 )
		} ) ) ;
	} ;
	render ( ) {
		return (
			<div>
				<div className='calculator-body'>
					<ResultComponent result={this.state.result}/>
					<KeyPadComponent onClick={this.onClick}/>
				</div>
			</div>
		) ;
	}
}
