import path		from 'path' ;

import express		from 'express' ;
// import mongoose		from 'mongoose' ;

import Word from './word' ;

const	app	= express ( ) ;
//	mongoDB	= 'mongodb+srv://dcastellon:Awesome0001@cluster0-0qm90.azure.mongodb.net/Toys?retryWrites=true' ;

// mongoose . connect ( mongoDB , { useNewUrlParser : true } ) ;

app . use ( express . urlencoded ( { extended : false } ) ) ;
app . use ( express . json ( ) ) ;

app . get ( '/find/:word' , ( req , res , next ) => {
	const search = new RegExp ( req . params . word ) ;
	Word . find ( { _id : search } ) . exec ( ( err , text_list ) => {

		if ( err ) {
			return err ;
		}

		var matches = [ ] ;
		text_list . forEach ( ( value , index , array ) => {
			matches . push ( value . _id ) ;
		} ) ;
		res . send ( matches ) ;
	} ) ;
} ) ;

app . post ( '/create' , ( req , res , next ) => {
	const word = req . body . word ;
	console . log ( 'word: ' + word ) ;
	Word . create ( { _id : word } , ( err , something ) => {
		if ( err ) {
			if ( err . code == 11000 ) res . send ( word + ' is already in database' ) ;
			return err ;
		}
		res . send ( word + ' saved in database!' ) ;
	} ) ;
} ) ;

export default app ;
