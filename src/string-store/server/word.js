import mongoose from 'mongoose' ;
import moment from 'moment' ;

var Schema = mongoose . Schema ;


var WordSchema = new Schema ( { _id : String } ) ;

export default mongoose . model ( 'Word' , WordSchema , 'Words' ) ;
