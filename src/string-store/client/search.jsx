export default function search ( ) {
	var search = document . getElementById ( 'word-store-in' ) . value ;
	if ( search . length == 0 ) {
		document . getElementById ( 'word-store-out' ) . innerHTML = '' ;
		return ;
	}
	if ( window . XMLHttpRequest ) {
		var xmlhttp = new XMLHttpRequest ( ) ;
	}
	xmlhttp . onreadystatechange = function ( ) {
		if ( this . readyState == 4 && this . status == 200 ) {
			var response = JSON . parse ( this . responseText ) ;
			console . log ( 'response: ' + response ) ;
		
			const output = document . getElementById ( 'word-store-out' ) ;
			output . innerHTML = '' ;

			let index = 0 ;
			for ( const item of response ) {
				index = index + 1 ;
				if ( index > 5 ) break ;

				const element = document . createElement ( 'li' ) ;
				element . textContent = item ;
				output . appendChild ( element ) ;
			} 
		}
	}
	xmlhttp . open ( 'get' , '/toy/word-store/find/' + search , true ) ;
	xmlhttp . send ( ) ;
} ;
