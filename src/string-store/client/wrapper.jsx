import search from './search.jsx' ;
import create from './create.jsx' ;

// Initializes the Names Store App on the Client

var	word_store	= document . getElementById ( 'word-store' ) ,	// the form
	input		= document . createElement ( 'input' ) ,	// the form input
	output		= document . createElement ( 'ul' ) ,		// the server result
	submit		= document . createElement ( 'input' ) ;	// the server result

input . setAttribute ( 'id' , 'word-store-in' ) ;
input . setAttribute ( 'name' , 'word' ) ;
input . setAttribute ( 'type' , 'text' ) ;
input . setAttribute ( 'size' , '30' ) ;
input . setAttribute ( 'autocomplete' , 'off' ) ;

output . setAttribute ( 'id' , 'word-store-out' ) ;
output . setAttribute ( 'class' , 'list-unstyled' ) ;

submit . setAttribute ( 'id' , 'word-store-submit' ) ;
submit . setAttribute ( 'type' , 'submit' ) ;
submit . setAttribute ( 'value' , 'Submit' ) ;

word_store . addEventListener ( 'submit' , create ) ;

word_store . appendChild ( input ) ;
word_store . appendChild ( output ) ;
word_store . appendChild ( submit ) ;

input . onkeyup = search ;
