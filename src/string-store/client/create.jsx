export default function create ( event ) {
	event . preventDefault ( ) ;

	var xmlhttp = new XMLHttpRequest ( ) ;
	var response = { } ;

	response . word = document . getElementById ( 'word-store-in' ) . value ;

	if ( response . word . length == 0 ) {
		document . getElementById ( 'word-store-out' ) . textContent = 'Enter some text' ;
	}

	xmlhttp . onreadystatechange = function ( ) {
		if ( this . readyState == 4 && this . status == 200 ) {
			document . getElementById ( 'word-store-out' ) . textContent = this . responseText ;
		}
	} ;

	xmlhttp . open ( 'post' , '/toy/word-store/create' , true ) ;
	xmlhttp . setRequestHeader ( 'Content-type' , 'application/json' ) ;
	xmlhttp . send ( JSON . stringify ( response ) ) ;
} ;
