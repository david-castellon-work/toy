import path			from 'path'				;

import express			from 'express'				;
import mongoose			from 'mongoose'				;
import morgan			from 'morgan'				;


import string_store_app		from './string-store/server/app'	;
import login_app		from './login/server/app'		;

const	app = express ( ) ;

const mongoDB = 'mongodb+srv://dcastellon:Awesome0001@cluster0-0qm90.azure.mongodb.net/Toys?retryWrites=true' ;

// const mongoDB = 'mongodb://159.65.191.124:27017/Toys?retryWrites=true' ;

mongoose . connect ( mongoDB , { useNewUrlParser : true } ) ;

app . set ( 'view engine' , 'pug' ) ;
app . set ( 'views' , path . join ( __dirname , 'views' ) ) ; 
app . use ( '/public' , express . static ( __dirname ) ) ;

app . use ( express . urlencoded ( { extended : false } ) ) ;
app . use ( express . json ( ) ) ;
app . use ( morgan ( 'dev' ) ) ;
app . get ( '/documentation' , ( req , res ) => {
	res . render ( 'toy' ) ;
} ) ;
// app . use ( '/' , ( ) => console . log ( 'inside /:' ) ) ;
app . use ( '/word-store' , string_store_app ) ;
app . use ( '/users' , login_app ) ;

export default app ;

function test_redirect ( req , res ) {
	res . redirect ( '/toy/documentation' ) ;
}
