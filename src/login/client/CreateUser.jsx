import React from 'react' ;
export default class CreateUser extends React . Component {
	constructor ( props ) {
		super ( props ) ;
		this . state = {
			username : 'Davy' ,
			password : 'Cas' ,
			passwordType : 'password' ,
			response : '' ,
		} ;
	}
	handleUsernameChange = ( ) => {
		this . setState ( { username : event . target . value } ) ;
		console . log ( 'this.response: ' + this . state . response ) ;
	}
	handlePasswordChange = ( ) => {
		this . setState ( { password : event . target . value } ) ;
	}
	handleSubmit = ( event ) => {
		event . preventDefault ( ) ;

		let xhr = new XMLHttpRequest ( ) ;
		let username = this . state . username ;
		let password = this . state . password ;

		let request = { } ;
		request . username = this . state . username ;
		request . password = this . state . password ;

		// for some reason on ready state change is giving issues
		xhr . onreadystatechange = ( param ) => {
			return function ( classThis ) {
				if ( this . readyState == 4 && this . status == 200 ) {
					classThis . setState ( { response : this . responseText } ) ;
					console . log ( 'classThis . state . response: ' + classThis . state . response ) ;
				}
			} . bind ( xhr ) ( this ) ;
		} ;

		xhr . open ( 'post' , '/toy/users/create' , true ) ;
		xhr . setRequestHeader ( 'Content-type' , 'application/json' ) ;

		request = JSON . stringify ( request ) ;
		xhr . send ( request ) ;


	}
	changePasswordType = ( ) => {
		if ( this . state . passwordType === 'password' ) {
			this . setState ( { passwordType : 'text'  } ) ;
		} else {
			this . setState ( { passwordType : 'password'  } ) ;
		}
	}
	render ( ) {
		return (
			<form onSubmit={ this . handleSubmit } className='create-user'>
				<label>
					Username:
					<input
						type = "text"
						value = { this . state . username }
						onChange = { this . handleUsernameChange }
					/><br />
					Password:
					<input
						type = { this . state . passwordType }
						value = { this . state . password }
						onChange = { this . handlePasswordChange }
					/>
					<input
						type = "checkbox"
						onClick = { this . changePasswordType }
					/><br />
				</label>
				<div id = 'login-output'>{ this . state . response }</div>
				<input type = "submit" value = "Submit" />
			</form>
		) ;
	}
} ;
