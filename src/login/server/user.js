import mongoose from 'mongoose' ;
import moment from 'moment' ;

var Schema = mongoose . Schema ;

var UserSchema = new Schema ( {
	// The id is the username
	_id : String ,
	password : String ,
} ) ;

export default mongoose . model ( 'User' , UserSchema , 'Users' ) ;
