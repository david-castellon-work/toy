import path		from 'path' ;

import express		from 'express' ;

import Users from './user' ;

const	app	= express ( ) ;

app . use ( express . urlencoded ( { extended : false } ) ) ;
app . use ( express . json ( ) ) ;
// Handles User Login
// I need to add the password part, but I'm just checking at the moment.
app . post ( '/find' , ( req , res , next ) => {
	const username = new RegExp ( req . body . username ) ;
	Users . find ( { _id : search } ) . exec ( ( err , matches ) => {
		if ( err ) return err ;
		if ( matches [ 0 ] == search ) {
			console . log ( 'User in database!' ) ;
			res . send ( 'User in database!' ) ;
		} else {
			console . log ( 'User not in database!' ) ;
			res . send ( 'User not in database!' ) ;
		}
	} ) ;
} ) ;
// Handle User Creation
// I need to be able to create the user.
app . post ( '/create' , ( req , res , next ) => {
	console . log ( 'in /users/create' ) ;
	const username = req . body . username ;
	const password = req . body . password ;
	console . log ( 'username: ' + username ) ;
	console . log ( 'password: ' + password ) ;
	Users . create ( {
		_id : username ,
		password : password
	} , ( err , something ) => { if ( err ) {
		console . log ( 'something: ' + something ) ;
		if ( err . code == 11000 ) res . send ( username + ' is already in database' ) ;
		return err ;
	}
		res . send ( username + ' saved in database!' ) ;
	} ) ;
} ) ;
export default app ;
