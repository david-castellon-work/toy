import React from 'react' ;

export default class LoggingButton extends React . Component {
	handleClick = ( ) => {
		// I think that because this is tied to an event
		// this cannot propogate out 
		console . log ( 'this is:' , this ) ;
	}
	render ( ) {
		// this.handleClick refers to the text that defines the
		// function, so you need to bind this
		return (
			<button onClick={this.handleClick}>
				Click Me
			</button>
		) ;
	}

} ;
