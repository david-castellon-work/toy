import React from 'react' ;
import ReactDOM from 'react-dom' ;

// The value attribute is set on our form element, so 
// the displayed value will always be this . state . value,
// making the React state the source of truth
//
// The onChange attribute has handleChange tied to it so
// every keystroke updates the react state.
export default class NameForm extends React . Component {
	constructor ( props ) {
		super ( props ) ;
		this . state = { value : '' } ;
	}
	handleChange = ( event ) => {
		this . setState ( { value : event . target . value } ) ;
	}
	handleSubmit = ( event ) => {
		alert ( 'A name was submitted: ' + this . state . value ) ;
		event . preventDefault ( ) ;
	}
	render ( ) {
		return (
			<form onSubmit = { this . handleSubmit }>
				<label>
					Name:
					<input
						type = "text" ,
						value = { this . state . value } ,
						onChange = { this . handleChange }
					/>
				</label>
				<input type = "submit" value = "Submit" />
			</form>
		) ;
	}
} ;
export class EssayForm extends React . Component {
	constructor ( props ) {
		super ( props ) ;
		this . state = {
			value : 'Please write an essay about your favorite DOM element.'
		} ;
	}
	handleChange = ( event ) => {
		this . setState ( { value : event . target . value } ) ;
	}
	handleSubmit = ( event ) => {
		alert ( 'An essay was submitted: ' + this . state . value ) ;
		event . preventDefault ( ) ;
	}
	render ( ) {
		return (
			<form onSubmit = { this . handleSubmit }>
				<label>
					Essay
					<textarea
						value = { this . state . value } ,
						onChange = { this . handleChange }
					/>
				</label>
				<input
					type = "submit" ,
					value = "Submit"
				/>
			</form>
		) ;
	}
} ;
class FlavorForm extends React . Component {
	constructor ( props ) {
		super ( props ) ;
		this . state = { value : 'cocunut' } ;
	}
	handleChange = ( event ) => {
		this . setState ( { value : event . target . value } ) ;
	}
	handleSubmit = ( event ) => {
		alert ( 'Favorite flavor: ' + this . state . value ) ;
		event . preventDefault ( ) ;
	}
	render ( ) {
		return (
			<form onSubmit = { this . handleSubmit }>
				<label>
					Pick your favorite flower:
					<select
						value = { this . state . value } ,
						onChange = { this . handleChange }
					>
							<option value = "grapefruit">Grapefruit</option>
							<option value = "lime">Lime</option>
							<option value = "Coconut">Coconut</option>
							<option value = "Mango">Mango</option>
					</select>
				</label>
				<input type = "submit" value = "Submit" />
			</form>
		) ;
	}

