import express from 'express' ;

import app from './app.js' ;

var server = express ( ) ;
const port = process . env . PORT || '3000' ;

server . use ( '/toy' , app ) ;
server . listen ( port ) ;

function test_redirect ( req , res ) {
	res . redirect ( '/toy' ) ;
}
