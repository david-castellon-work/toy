var	path = require ( 'path' ) ;
var	browserify = require ( 'browserify' ) ,
	gulp = require ( 'gulp' ) ,
	source = require ( 'vinyl-source-stream' ) ,
	sourcemaps = require ( 'gulp-sourcemaps' ) ,
	buffer = require ( 'vinyl-buffer' ) ,
	babel = require ( 'gulp-babel' ) ,
	babelify = require ( 'babelify' ) ,
	sass = require ( 'gulp-sass' ) (require ('node-sass') ) ;

gulp . task ( 'views' , function ( ) {
	return gulp . src ( 'src/views/*' )
	. pipe ( gulp . dest ( 'build/views' ) ) ;
} ) ;
gulp . task ( 'styles' , function ( ) {
	return gulp . src  ( [
		'node_modules/dark/src/dark.scss' ,
		'src/styles/tictactoe.scss' ,
		'src/calculator/calculator.scss' ,
	] )
	. pipe ( sass ( ) . on ( 'error' , sass . logError ) )
	. pipe ( gulp . dest ( 'build/styles' ) ) ;
} ) ;
gulp . task ( 'server_scripts' , function ( ) {
	return gulp . src ( [ 'src/**/*.js' ] )
	. pipe ( babel ( {
		presets : [
			'@babel/preset-env' ,
		]
	} ) )
	. pipe ( gulp . dest ( 'build' ) ) ;
} ) ;
gulp . task ( 'client_scripts' , function ( ) {
	return browserify ( {
		entries : [
			'src/tictactoe/wrapper.jsx' ,
			'src/string-store/client/wrapper.jsx' ,
			'src/clock/wrapper.jsx' ,
			'src/calculator/wrapper.jsx' ,
			// 'src/report_this/wrapper.jsx' ,
			'src/login/client/wrapper.jsx'
		]
	} )
	. transform ( babelify . configure ( {
		presets : [
			'@babel/preset-env' ,
			'@babel/preset-react' ,
		] ,
		plugins : [
			'@babel/plugin-proposal-class-properties' ,
		]
	} ) ) . bundle ( )
	. pipe ( source ( 'scripts/toys.js' ) )
	. pipe ( buffer ( ) )
	. pipe ( sourcemaps . init ( { loadMaps : true } ) )
	. pipe ( sourcemaps . mapSources ( function ( sourcePath , file ) {
		return path . resolve ( 'src' , sourcePath ) ;
	} ) )
	. pipe ( sourcemaps . write ( './maps' , {
		includeContent : false
	} ) )
	. pipe ( gulp . dest ( 'build/' ) ) ;
} ) ;
gulp . task ( 'default' , gulp . series ( 'views' , 'styles' , 'server_scripts' , 'client_scripts' ) ) ;
